# -*- coding: utf-8 -*-
"""
Created on Tue Apr 27 10:41:47 2021

@author: Jason George Velentzas
@location: Thessaloniki, Greece
"""

# import time
from graphics import *
import palettes as pal


color_palette = pal.colors
num_palette = pal.numbers
m = len(num_palette)
n = len(num_palette[0])
bg_1 = pal.background_color_1
bg_2 = pal.background_color_2
bg_green = pal.green_color
bg_line = pal.line_color
clr_m = len(color_palette)
clr_n = len(color_palette[0])
grid_pos = []
num_pos = []
palette_pos = []


class Table:
    def __init__(self, dim1, dim2):
        global bg_1
        self.M = dim1
        self.N = dim2
        self.offset_x = 240
        self.offset_y = 120
        self.x0 = self.offset_x
        self.y0 = self.offset_y
        self.color_size = 35
        if self.M < 9 and self.N < 14:
            self.cell_size = 50
        else:
            self.cell_size = 40
        self.xf = self.x0 + self.N * self.cell_size
        self.yf = self.y0 + self.N * self.cell_size
        if self.yf < clr_m * self.color_size + self.offset_y:
            self.offset_y += (clr_m * self.color_size + self.offset_y - self.yf)/2
        self.regs = {}
        self.cells = []
        for row in range(clr_m):
            for col in range(clr_n):
                self.regs[color_palette[row][col]] = 0

        for row in range(self.M):
            cell_row = []
            for col in range(self.N):
                cell_row.append(Cell(row, col))
            self.cells.append(cell_row)
        # Calculate neighbors
        for row in range(self.M):
            for col in range(self.N):
                # calculate neigbors
                if row > 0:
                    self.cells[row][col].neighbors.add(self.cells[row - 1][col])
                    if col > 0:
                        self.cells[row][col].neighbors.add(self.cells[row - 1][col - 1])
                    if col < self.N - 1:
                        self.cells[row][col].neighbors.add(self.cells[row - 1][col + 1])
                if row < self.M - 1:
                    self.cells[row][col].neighbors.add(self.cells[row + 1][col])
                    if col > 0:
                        self.cells[row][col].neighbors.add(self.cells[row + 1][col - 1])
                    if col < self.N - 1:
                        self.cells[row][col].neighbors.add(self.cells[row + 1][col + 1])
                if col > 0:
                    self.cells[row][col].neighbors.add(self.cells[row][col - 1])
                if col < self.N - 1:
                    self.cells[row][col].neighbors.add(self.cells[row][col + 1])

        self.win = GraphWin('Puzzle Solution', self.xf + self.offset_x, self.yf + self.offset_y)
        self.win.setBackground(bg_1)


class Grid:
    def __init__(self, tbl):
        self.p1 = Point(tbl.x0, tbl.y0)
        self.p2 = Point(tbl.xf, tbl.yf)
        self.rec = Rectangle(self.p1, self.p2)
        self.rec.setFill('white')
        self.rec.setOutline('black')
        self.rec.setWidth(12)
        self.rec.draw(tbl.win)


class Cell:

    def __init__(self, row, col):
        self.row = row
        self.col = col
        self.val = -1
        self.reg = 0
        self.poss = []
        self.neighbors = set()


def plot_grid(tbl):
    global bg_line
    global bg_2
    global bg_1
    global bg_green
    for row in range(tbl.M):
        grid_row = []
        for col in range(tbl.N):
            x1 = tbl.x0 + col * tbl.cell_size
            y1 = tbl.y0 + row * tbl.cell_size
            point1 = Point(x1, y1)
            point2 = Point(x1 + tbl.cell_size, y1 + tbl.cell_size)
            rec = Rectangle(point1, point2)
            rec.draw(tbl.win)
            rec.setOutline(bg_line)
            rec.setFill(bg_2)
            rec.setWidth(2)
            grid_row.append((x1, y1))
        grid_pos.append(grid_row)
    xc = (tbl.offset_x - len(color_palette[0]) * tbl.color_size) / 2
    yc = (tbl.yf + tbl.offset_y - len(color_palette) * tbl.color_size) / 2
    for row in range(len(color_palette)):
        palette_row = []
        num_row = []
        for col in range(len(color_palette[0])):
            x1 = xc + col * tbl.color_size
            y1 = yc + row * tbl.color_size
            point1 = Point(x1, y1)
            point2 = Point(x1 + tbl.color_size, y1 + tbl.color_size)
            rec = Rectangle(point1, point2)
            rec.draw(tbl.win)
            rec.setOutline(bg_line)
            rec.setFill(color_palette[row][col])
            rec.setWidth(2)
            palette_row.append((x1, y1))
            x2 = tbl.xf + xc + col * tbl.color_size
            y2 = yc + row * tbl.color_size
            point1 = Point(x2, y2)
            point2 = Point(x2 + tbl.color_size, y2 + tbl.color_size)
            rec = Rectangle(point1, point2)
            rec.draw(tbl.win)
            rec.setOutline(bg_line)
            rec.setFill('white')
            rec.setWidth(2)
            label = Text(Point(x2 + tbl.color_size / 2, y2 + tbl.color_size / 2), num_palette[row][col])
            label.setStyle('bold')
            label.setFill(bg_line)
            label.setSize(12)
            label.draw(tbl.win)
            num_row.append((x2, y2))
        num_pos.append(num_row)
        palette_pos.append(palette_row)

    sub_btn_X_size = 80
    sub_btn_Y_size = 50
    btn_1_x = tbl.xf + (tbl.offset_x - sub_btn_X_size) / 2
    btn_1_y = tbl.yf + (tbl.offset_y - sub_btn_Y_size) / 2
    btn_Point_1 = Point(btn_1_x, btn_1_y)
    btn_Point_2 = Point(btn_1_x + sub_btn_X_size, btn_1_y + sub_btn_Y_size)
    sub_btn = Rectangle(btn_Point_1, btn_Point_2)
    sub_btn.setOutline(bg_line)
    sub_btn.setWidth(4)
    sub_btn.draw(tbl.win)
    sub_btn.setFill(bg_green)
    label = Text(Point(btn_1_x + sub_btn_X_size / 2, btn_1_y + sub_btn_Y_size / 2), 'Submit')
    label.setFill(bg_line)
    label.setSize(16)
    label.draw(tbl.win)
    label = Text(Point((tbl.xf + tbl.offset_x) / 2, tbl.offset_y / 2), 'Fill in Colors(Regions) and Numbers')
    label.setFill(bg_line)
    label.setSize(20)
    label.setStyle('bold')
    label.draw(tbl.win)
    return btn_Point_1, btn_Point_2


def plot_solution(p1_button, p2_button, tbl):
    for c_r in tbl.cells:
        for cell in c_r:
            x = tbl.x0 + (cell.col + 0.5) * tbl.cell_size
            y = tbl.y0 + (cell.row + 0.5) * tbl.cell_size
            point = Point(x, y)
            label = Text(point, str(cell.val))
            label.setFill('black')
            label.setFace('courier')
            label.setSize(18)
            label.draw(tbl.win)
    rec_button = Rectangle(p1_button, p2_button)
    rec_button.setOutline('black')
    rec_button.setWidth(4)
    rec_button.draw(tbl.win)
    rec_button.setFill(color_rgb(0, 204, 102))
    label = Text(Point((p1_button.x+p2_button.x)/2, (p1_button.y+p2_button.y)/2), 'Close')
    label.setFill('white')
    label.setSize(16)
    label.draw(tbl.win)


def col_num_selected(point, pos):
    dim1 = len(pos)
    dim2 = len(pos[0])
    if pos[0][0][0] < point.x < pos[dim1 - 1][dim2 - 1][0] + table.color_size and \
            pos[0][0][1] < point.y < pos[dim1 - 1][dim2 - 1][1] + table.color_size:
        return True
    return False


def find_selected_col_num(point, pos):
    for dim1 in range(len(pos)):
        for dim2 in range(len(pos[0])):
            q = pos[dim1][dim2]
            if q[0] <= point.x <= q[0] + table.color_size and q[1] <= point.y <= q[1] + table.color_size:
                return dim1, dim2


def user_interaction(point1, point2, tbl):

    global bg_line
    point = Point(0, 0)
    last_selection = []

    while not (point1.x < point.x < point2.x and point1.y < point.y < point2.y):
        point = tbl.win.getMouse()
        if col_num_selected(point, palette_pos):
            # Color selected
            row, col = find_selected_col_num(point, palette_pos)
            color_selected = color_palette[row][col]
            last_selection = (color_selected, 'col')
        elif col_num_selected(point, num_pos):
            # Number selected
            row, col = find_selected_col_num(point, num_pos)
            number_selected = num_palette[row][col]
            last_selection = (number_selected, 'n')
        elif col_num_selected(point, grid_pos):
            if last_selection:
                if last_selection[1] == 'col':
                    if find_selected_col_num(point, grid_pos):
                        row, col = find_selected_col_num(point, grid_pos)
                        x1 = tbl.x0 + col * tbl.cell_size
                        y1 = tbl.y0 + row * tbl.cell_size
                        p11 = Point(x1, y1)
                        p22 = Point(x1 + tbl.cell_size, y1 + tbl.cell_size)
                        rec = Rectangle(p11, p22)
                        rec.setOutline(bg_line)
                        rec.setWidth(2)
                        rec.setFill(last_selection[0])
                        rec.draw(tbl.win)
                        tbl.cells[row][col].reg = last_selection[0]
                elif last_selection[1] == 'n':
                    if find_selected_col_num(point, grid_pos):
                        row, col = find_selected_col_num(point, grid_pos)
                        x2 = tbl.x0 + col * tbl.cell_size
                        y2 = tbl.y0 + row * tbl.cell_size
                        label = Text(Point(x2 + tbl.cell_size / 2, y2 + tbl.cell_size / 2), last_selection[0])
                        label.setStyle('bold')
                        label.setFill(bg_line)
                        label.setSize(18)
                        label.draw(tbl.win)
                        tbl.cells[row][col].val = last_selection[0]
    return True


def find_next_move(tbl):

    for row in range(tbl.M):
        for col in range(tbl.N):
            if tbl.cells[row][col].val == -1:
                return row, col
    return -1, -1


def is_valid(i, j, tbl):
    for row in range(tbl.M):
        for col in range(tbl.N):
            if tbl.cells[row][col].val > -1:
                value = tbl.cells[row][col].val
                for neighbor in tbl.cells[row][col].neighbors:
                    if value == neighbor.val:
                        return False
    myRegion = tbl.cells[i][j].reg
    myValue = tbl.cells[i][j].val
    for row in range(tbl.M):
        for col in range(tbl.N):
            if myRegion == tbl.cells[row][col].reg:
                if row != i or col != j:
                    if tbl.cells[row][col].val == myValue:
                        return False
    return True


def solve_pzl(tbl):
    i, j = find_next_move(tbl)
    if i == -1:
        print("solved")
        return True
    for e in tbl.cells[i][j].poss:
        tbl.cells[i][j].val = e
        if is_valid(i, j, tbl):
            if solve_pzl(tbl):
                return True
        tbl.cells[i][j].val = -1
    return False    

    
if __name__ == "__main__":
    flag1 = False
    flag2 = False
    while not flag1:
        val1 = input("Enter number of rows: \n")
        if val1.isnumeric():
            if int(val1) > 0:
                flag1 = True
            else:
                print("Please enter a non-negative value! \n")
        else:
            print("Please enter an integer value \n")

    while not flag2:
        val2 = input("Enter number of columns: \n")
        if val2.isnumeric():
            if int(val2) > 0:
                flag2 = True
            else:
                print("Please enter a non-negative value! \n")
        else:
            print("Please enter an integer value \n")

    table = Table(int(val1), int(val2))    p1, p2 = plot_grid(table)
    if user_interaction(p1, p2, table):
        # This means the user has submitted data. We can create poss lists
        # Update regs and cells.poss
        for r in range(table.M):
            for c in range(table.N):
                table.regs[table.cells[r][c].reg] += 1
        table.regs = {x: y for x, y in table.regs.items() if y != 0}
        for r in range(table.M):
            for c in range(table.N):
                for key in table.regs:
                    if table.cells[r][c].reg == key:
                        table.cells[r][c].poss = list(range(1, table.regs[key] + 1))
        start_time = time.time()
        solve_pzl(table)
        print(time.time() - start_time)
        plot_solution(p1, p2, table)
        p = table.win.getMouse()
        while not (p1.x < p.x < p2.x and p1.y < p.y < p2.y):
            p = table.win.getMouse()
        table.win.close()
