# My Puzzles

A project about automating the procedure of solving various kinds of puzzles/riddles.


## Puzzle-1: SUGURU

This puzzle caught my eye from the beginning of my involvement with riddles and puzzles. The reason? Its majestic simplicity! 

#### Rules

The rules of the puzzle are quite straightforward. There is a grid divided into regions (normally through bolding bording lines, but here for UI purposes with different cell colors). Each cell must be filled with a number, so that:

-- each region R_t contains all numbers fron 1 to N_t (where N_t is the number of cells of region R_t)

-- a cell does not have the same number with any other neighbouring cell touching it (not even diagonally).

#### SUGURU-SOLVER

In order to solve suguru you just need to run the "suguru.py" script.

Firstly you need to enter the desired number of rows and columns and then fill the grid creating the puzzle. 

Pressing submit button will solve the puzzle and then pressing close button will print the time needed to solve the riddle in secs.

_Note1: There must be at least one number provided in the grid. This shall be fixed in a later version_

_Note2: If a number/color is selected and the click on the grid doesn't work, just click again! :) Usually it takes one click, but it can take some time sometimes_

_Note3: The graphics work well for either approximately balanced number of rows and columns or a sufficient number of columns (for example 10 rows and 3 columns won't produce a satisfactory result). In any case the inputs must be reasonable. Hopefulle it shall be fixed in the future._

_Note4: The UI is based on the [graphics.py](http://anh.cs.luc.edu/handsonPythonTutorial/graphics.html) module by John Zelle_

#### DEMO

[Here](demos/_DEMO_Suguru_8x8.avi) you can find a demo of the puzzle as can be seen in the photo below:

<img src="demo_screenshot.png"
     alt="Suguru Solved 8x8 Demo"
     width = 40% />
